


#ifndef _BASE_CRC16_H_
#define _BASE_CRC16_H_

#include <stdint.h>
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus  */

/**
 * @brief 通过查表法 按字节长度计算获取CRC16XMODEM校验码
 *
 * @param p_buf 进行CRC检验的数据
 * @param len 进行CRC检验的数据长度
 * @return uint16_t 生成的CRC校验码
 */
uint16_t get_crc16_code(const char* p_buf, int len);

#ifdef __cplusplus
}
#endif  /* __cplusplus  */
#endif  // _BASE_CRC16_H_
