/**
 * @file spin_mutex.h
 * @brief 自旋锁封装
 * @copyright Copyright (c) 2020 YUSUR Technology Co., Ltd. All Rights Reserved. Learn more at www.yusur.tech.
 * @author Renjie Qi (qirj@yusur.tech)
 * @date 2023-01-09 19:14:13
 * @last_author: Renjie Qi (qirj@yusur.tech)
 * @last_edit_time: 2023-01-09 19:14:37
 */

#ifndef _BASE_SPIN_MUTEX_H_
#define _BASE_SPIN_MUTEX_H_

#include <pthread.h>
#include <mutex> // lock_guard & unique_lock

// C++ std style
class SpinMutex
{
public:
    SpinMutex()
    {
        pthread_spin_init(&spin_lock_, 0);
    }

    ~SpinMutex()
    {
        pthread_spin_destroy(&spin_lock_);
    }

    SpinMutex(const SpinMutex&) = delete;
    SpinMutex& operator= (const SpinMutex&) = delete;

    void lock()
    {
        pthread_spin_lock(&spin_lock_);
    }

    void unlock()
    {
        pthread_spin_unlock(&spin_lock_);
    }

private:
    pthread_spinlock_t spin_lock_;
};

#endif // _BASE_SPIN_MUTEX_H_
