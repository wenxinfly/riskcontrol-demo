

#ifndef _BASE_TIME_UTILS_
#define _BASE_TIME_UTILS_

#include <stdint.h>
#include <string>

/**
 * @brief 获取当前时间微秒级时间戳
 *
 * @return int64_t 当前时间微秒级时间戳
 */
int64_t get_current_timestamp_us();

/**
 * @brief 获取当前时间毫秒级时间戳
 *
 * @return int64_t 当前时间毫秒级时间戳
 */
int64_t get_current_timestamp_ms();

/**
 * @brief 获取当前时间秒级时间戳
 *
 * @return int64_t 当前时间秒级时间戳
 */
int64_t get_current_timestamp_s();

/**
 * @brief 转换HHMMSSsss格式为毫秒级时间戳
 * @param mdtime HHMMSSsss格式时间
 * @return 毫秒级时间戳
 */
int64_t convert_to_timestamp_ms(uint64_t mdtime);

/**
 * @brief 转换 HHMMSS 格式为，距基准时间秒级时间戳
 * @param mdtime HHMMSS格式时间
 * @return 秒级时间戳，如果转换失败，返回-1
 */
int64_t convert_to_timestamp_after_basetime_s(uint64_t mdtime);
int64_t convert_to_timestamp_after_basetime_s(int hour, int minute, int second);

/**
 * @brief 获取具体时间的毫秒时间戳
 *
 * @param year    年
 * @param month   月
 * @param day     日
 * @param hour    时
 * @param minute  分
 * @param second  秒
 * @return int64_t 毫秒级时间戳，如果转换失败，返回-1
 */
int64_t get_timestamp_from_date_ms(int year, int month, int day, int hour, int minute, int second);

/**
 * @brief 获取当天某个时间点的毫秒时间戳
 *
 * @param hour     时
 * @param minute   分
 * @param second   秒
 * @return int64_t 毫秒级时间戳，如果转换失败，返回-1
 */
int64_t get_timestamp_today_ms(int hour, int minute, int second);

/**
 * @brief 检查格式，若合法，转换HHMMSS格式为微秒级时间戳
 *
 * @param mdtime
 * @param target_timestamp
 * @return true
 * @return false 格式不合法，H > 24 || M > 59 || S > 59
 */
bool check_and_convert_to_timestamp_us(uint64_t mdtime, uint64_t& target_timestamp);

/**
 * @brief 返回当前日期 格式为 YYYYMMDD
 *
 * @return const char*
 */
int64_t get_current_format_date();

/**
 * @brief 返回当前时间的字符串 默认格式为 YYYYMMDD-ddmmss
 *
 * @return std::string
 */
std::string get_current_datetime_str(const std::string& format = "%04d%02d%02d-%02d%02d%02d");

/**
 * @brief 将微妙级时间戳转化为日志格式字符串
 * @param tt 时间戳
 * @return 日志格式字符串
 */
std::string timestamp_to_string_us(time_t tt);

/**
 * @brief 忙等待若干微秒（有微秒级误差，ut使用）
 */
void wait_for_us(uint32_t time);

/**
 * @brief 返回当前时间的字符串 格式为 YYYY.MM.DD dd:mm:ss.f
 *
 * @return std::string 当前时间的字符串
 */
std::string get_current_datetime_us_str();

// 将微秒时间戳转换为当天的秒数
uint32_t convert_to_seconds_in_day(uint64_t timestamp_us);

#endif  // _BASE_TIME_UTILS_
