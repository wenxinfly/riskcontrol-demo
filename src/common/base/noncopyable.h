/**
 * @file nocopyable.h
 * @brief To-be-added
 * @copyright Copyright (c) 2020 YUSUR Technology Co., Ltd. All Rights Reserved. Learn more at
 * www.yusur.tech.
 * @author Qiang Wang (wangq@yusur.tech)
 * @date 2021-09-06 15:46:28
 * @last_author: TianXing Qu (qutx@yusur.tech)
 * @last_edit_time: 2022-03-10 14:24:31
 */

#ifndef _BASE_NONCOPYABLE_H_
#define _BASE_NONCOPYABLE_H_


class NonCopyable
{
protected:
    NonCopyable() = default;
    ~NonCopyable() = default;

public:  // emphasize the following members are private
    NonCopyable(const NonCopyable&) = delete;
    const NonCopyable& operator=(const NonCopyable&) = delete;
};

#endif  // _BASE_NONCOPYABLE_H_
