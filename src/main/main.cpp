
#include <iostream>
#include  "riskcon_client.h"

int main(int argc, const char** argv)
 {
    if(argc != 4) {
        cout << "usage: echo_client NAME SERVER_IP USE_SHM[0|1]" << endl;
        exit(1);
    }
    const char* name = argv[1];
    const char* server_ip = argv[2];
    bool use_shm = argv[3][0] != '0';

    int  num = sysconf(_SC_NPROCESSORS_CONF);
    printf("cpu num = %d\n", num);

    RiskConClient client(name, name);
    client.Run(use_shm, server_ip, 9100);

    return 0;
}