set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)

include_directories (${CMAKE_SOURCE_DIR}/src)
include_directories (${CMAKE_SOURCE_DIR}/src/common)
include_directories (${CMAKE_SOURCE_DIR}/src/main)
#include_directories (${CMAKE_SOURCE_DIR}/src/adapt_data_pb)

aux_source_directory(. SRC_FILES)
message(STATUS "CMAKE_SOURCE_FILES: ${SRC_FILES}")

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR})

message(STATUS "CMAKE_BINARY_DIR: ${CMAKE_BINARY_DIR}")
message(STATUS "CMAKE_SOURCE_DIR: ${CMAKE_SOURCE_DIR}")
message(STATUS "RISKCON_DEMO: ${RISKCON_DEMO}")

add_executable(${PROJECT_NAME} ${SRC_FILES})

