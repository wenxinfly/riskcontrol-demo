#pragma once

#include "tcpshm/tcpshm_client.h"
#include "common.h"

using namespace std;
using namespace tcpshm;

struct ClientConf : public CommonConf
{
    static const int64_t NanoInSecond = 1000000000LL;

    static const uint32_t TcpQueueSize = 2000;       // must be a multiple of 8
    static const uint32_t TcpRecvBufInitSize = 1000; // must be a multiple of 8
    static const uint32_t TcpRecvBufMaxSize = 2000;  // must be a multiple of 8
    static const bool TcpNoDelay = true;

    static const int64_t ConnectionTimeout = 10 * NanoInSecond;
    static const int64_t HeartBeatInverval = 3 * NanoInSecond;

    using ConnectionUserData = char;
};

class RiskConClient;
using TSClient = TcpShmClient<RiskConClient, ClientConf>;

class RiskConClient : public TSClient
{
public:
    RiskConClient(const std::string& ptcp_dir, const std::string& name);
    void Run(bool use_shm, const char* server_ipv4, uint16_t server_port);
    bool SendMsg(int ntype, char* pdata, int nlen);
private:
    template <class T>
    void handleMsg(T *msg)
    {

        // printf("recv one pach time  %d--%d\n",cur - msg->val[1],cur - msg->val[0]);
    }

private:
    friend TSClient;

    void OnSystemError(const char *error_msg, int sys_errno);

    void OnLoginReject(const LoginRspMsg *login_rsp);

    int64_t OnLoginSuccess(const LoginRspMsg* login_rsp);

    void OnSeqNumberMismatch(uint32_t local_ack_seq,
                             uint32_t local_seq_start,
                             uint32_t local_seq_end,
                             uint32_t remote_ack_seq,
                             uint32_t remote_seq_start,
                             uint32_t remote_seq_end);
    void OnDisconnected(const char *reason, int sys_errno);

    void OnServerMsg(MsgHeader *header);

private:
    Connection& conn;
};